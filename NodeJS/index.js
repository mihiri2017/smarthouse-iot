/**
 * Created by Arcans on 09/12/15.
 */
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
app.get('/styles/semantic.css', function(req, res){
  res.sendFile(__dirname + '/styles/semantic.css');
});
app.get('/styles/semantic.min.js', function(req, res){
  res.sendFile(__dirname + '/styles/semantic.min.js');
});
app.get('/js/jquery-1.11.3.js', function(req, res){
  res.sendFile(__dirname + '/js/jquery-1.11.3.js');
});
app.get('/js/jquery-2.1.4.js', function(req, res){
  res.sendFile(__dirname + '/js/jquery-2.1.4.js');
});
app.get('/',function(req,res){
    res.sendFile(__dirname+'/index.html');

})

io.on('connection',function(socket){
    console.log('one user connected '+socket.id);

    socket.on('message',function(data){
        var sockets = io.sockets.sockets;
        sockets.forEach(function(sock){
            if(sock.id != socket.id)
            {
               sock.emit('message',data);
               var obj = JSON.stringify(data);
               console.log(obj);
            }
        })
    })
    socket.on('disconnect',function(){
        console.log('one user disconnected '+socket.id);
    })
})



http.listen(8100,function(){
    console.log('server listening on port 8100');
})
