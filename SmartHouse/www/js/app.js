// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
var app = angular.module('starter', ['ionic', 'LocalStorageModule', 'btford.socket-io', 'angularMoment', 'starter.controllers'])

    .run(function($ionicPlatform, $rootScope, $localstorage, $ionicHistory, $location) {
        $ionicPlatform.ready(function() {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
                cordova.plugins.Keyboard.disableScroll(true);

            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });

        $rootScope.testVar = 123;
        //$localstorage.set('loginApp.userId', 1);

        var userId = $localstorage.get('loginApp.userId');

        if(typeof userId == 'undefined' || userId == ''){
            $ionicHistory.nextViewOptions({
                disableBack: true
            });

            $location.path('/login');
        }
        else{
            $rootScope.userId = userId;
        }

        $rootScope.$on('$locationChangeStart', function (event, next, current) {

            if(!$rootScope.userId){
                $ionicHistory.nextViewOptions({
                    disableBack: true
                });

                $location.path('/login');
            }

        })

    })

    .config(function($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('login', {
                url: '/login',
                templateUrl: 'templates/login-page.html',
                controller: 'LoginCtrl'
            })

            .state('button', {
                url: '/button',
                templateUrl: 'templates/button.html',
                controller: 'BtnCtrl'
            })

          ;
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('/login');
    });
